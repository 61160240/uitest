# สมัครงานสำเร็จ อายุในช่วง ประสบการณ์มากกว่าที่กำหนด เพศตรง
*** Settings ***
Library    Selenium2Library

*** Variables ***


*** Test Cases ***
สมัครงานสำเร็จ
    เปิดเว็บตามลิ้งค์
    พิมพ์ในช่องค้นหา "Programmer" และกดปุ่มค้นหา
    ขึ้นอาชีพมา 4 จำนวน
    เลือกตำแหน่งงานที่1
    กดสมัคร
    ระบบแสดงว่า สมัครงานสำเร็จ

*** Keywords ***
เปิดเว็บตามลิ้งค์
    Open Browser          http://localhost:3000/searchJob    chrome
    Set Selenium Speed    0.5
พิมพ์ในช่องค้นหา "Programmer" และกดปุ่มค้นหา
    Input Text       id=search_text      Programmer
    Click Element    id=search_button
ขึ้นอาชีพมา 4 จำนวน
    Element Text Should Be    id=name_1    Python Programmer
เลือกตำแหน่งงานที่1
    Click Element                  id=show_detail_1
    Wait Until Element Contains    id=name             Python Programmer
กดสมัคร
    Click Element    id=apply_job
ระบบแสดงว่า สมัครงานสำเร็จ
    Element Should Contain    id=message    Applied Job